; Nev: Székely Szilárd Lehel
; Azonosito: ssim1961
; Csoportszam: 514

; Compile:
; nasm -f win32 mandelb.asm
; nlink mandelb.obj -lio -lgfx -o mandelb.exe

; WASD mozgas
; 1/2 pontossag 
; 3 szin 
; Q/E zoom
; Z/X iteracio noveles/csokkentes

%include 'io.inc'
%include 'gfx.inc'

%define WIDTH  1024
%define HEIGHT 768
%define CIKLUSVEGE [hatar]

global main

section .text

main:
    mov		eax, WIDTH												; window width (X)
	mov		ebx, HEIGHT												; window height (Y)
	mov		ecx, 0													; window mode (NOT fullscreen!)
	mov		edx, caption											; window caption
	call	gfx_init
	
	test	eax, eax												; if the return value is 0, something went wrong
	jnz		.init
	
; Print error message and exit
	
	mov		eax, errormsg
	call	io_writestr
	call	io_writeln
	ret
	
	.init:
		mov		eax, infomsg										; print some usage info
		call	io_writestr
		call	io_writeln
		
		xor		esi, esi											; deltax (used for moving the image)
		xor		edi, edi											; deltay (used for moving the image)
		
; Main loop
	
.mainloop:
	call	gfx_map												; map the framebuffer -> EAX will contain the pointer
	xor		ecx, ecx											; ECX - line (Y)
	.yloop:														; Loop over the lines
		cmp		ecx, HEIGHT
		jge		.yend	
		push	ecx
		xor		edx, edx										; EDX - column (X)
		.xloop:													; Loop over the columns
			cmp		edx, WIDTH
			jge		.xend
			push	ebx
			mov		ebx, [pontossag]
			
			cmp		ebx, 2										; ha 1 single, ha 2 double
			je		.double
			pop		ebx
			call	SingleKoordinata							; koordináták meghatározása
			push	edx
			call	SingleSzamol								; mandelbrot számolás
			call	SingleSzinez								; színezés
			pop		edx
			add		edx, 8
			
			jmp		.xloop
			
			.double:
			pop		ebx
			call	DoubleKoordinata
			push	edx
			call	DoubleSzamol
			call	DoubleSzinez
			pop		edx
			add		edx, 4
			
			jmp		.xloop
		.xend:
			pop		ecx
			inc		ecx
			jmp		.yloop
	.yend:
		call	gfx_unmap										; unmap the framebuffer
		call	gfx_draw										; draw the contents of the framebuffer (*must* be called once in each iteration!)
		
		xor		ebx, ebx										; load some constants into registers: 0, -1, 1
		mov		ecx, -5
		mov		edx, 5
	
.eventloop:
	call	gfx_getevent
	
	; Handle movement: keyboard
	cmp		eax, 'w'	; w key pressed
	cmove	edi, ecx	; deltay = -1 (if equal)
	cmp		eax, -'w'	; w key released
	cmove	edi, ebx	; deltay = 0 (if equal)
	cmp		eax, 's'	; s key pressed
	cmove	edi, edx	; deltay = 1 (if equal)
	cmp		eax, -'s'	; s key released
	cmove	edi, ebx	; deltay = 0
	cmp		eax, 'a'
	cmove	esi, ecx
	cmp		eax, -'a'
	cmove	esi, ebx
	cmp		eax, 'd'
	cmove	esi, edx
	cmp		eax, -'d'
	cmove	esi, ebx
	
	.jmp0: 		; nagyítás aktiv
		cmp     eax,'e'
		jne 	.jmp1
		mov 	[nagyit],byte 90	
	.jmp1:		; nagyítás vége
		cmp 	eax,-'e'
		jne 	.jmp2
		mov 	[nagyit],byte 100
	.jmp2:		; kicsinyítés aktiv
		cmp		 eax,'q'
		jne 	.jmp3
		mov 	[kicsinyit],byte 90
	.jmp3:		; kicsinyítés vége
		cmp 	eax,-'q'
		jne 	.jmp4
		mov 	[kicsinyit],byte 100
	.jmp4:		; iteráció növelés
		cmp 	eax,'z'
		jne 	.jmp5
		mov 	[hatarvalt], edx
	.jmp5:		; iteráció növelés vége
		cmp 	eax,-'z'
		jne 	.jmp6
		mov 	[hatarvalt], ebx
	.jmp6: 		; iteráció csökkentés
		cmp		 eax,'x'
		jne 	.jmp7
		mov 	[hatarvalt], ecx
	.jmp7:		; iteráció csökkentés vége
		cmp		eax, -'x'
		jne 	.jmp8
		mov 	[hatarvalt], ebx
	.jmp8:		; float pontosság
		cmp		eax, '1'
		jne		.jmp9
		mov		[pontossag], byte 1
	.jmp9:		; double pontosság
		cmp		eax, '2'
		jne		.jmp10
		mov		[pontossag], byte 2
	.jmp10:		; színváltás
		cmp		eax, '3'
		jne		.jmp11
		mov		[color], byte 50	
	.jmp11:		; színváltás vége
		cmp		eax, -'3'
		jne		.jmp12
		mov		[color], byte 0		
	.jmp12:
	
	; Handle exit
    cmp     eax, 23         ; the window close button was pressed: exit
    je      .end
    cmp     eax, 27         ; ESC: exit
    je      .end
    test    eax, eax        ; 0: no more events
    jnz     .eventloop
	
.updateoffset:

	; betöltöm a nagyitásokat
		movsd		 xmm0, [zoomx]
		movsd		 xmm1, [zoomy]
		vbroadcastsd ymm0, xmm0
		vbroadcastsd ymm1, xmm1
	
	; betöltöm a nagyítás változásokat
		vcvtsi2sd 	 xmm2, [nagyit]
		vcvtsi2sd	 xmm3, [kicsinyit]
		vbroadcastsd ymm2, xmm2
		vbroadcastsd ymm3, xmm3
	
	; kiszámolom az új nagyításokat és visszatöltöm a memóriába
		vmulpd 		ymm0, ymm2
		vmulpd 		ymm1, ymm2
		vdivpd 		ymm0, ymm3
		vdivpd 		ymm1, ymm3
		movsd 	[zoomx], xmm0
		movsd 	[zoomy], xmm1
 
	; x tengely szerint mozgatás, eltolás frissités
		vcvtsi2sd 		xmm3, esi
		mulsd			xmm3, [zoomx]
		vcvtsi2sd 		xmm0, [width]
		divsd 			xmm3, xmm0
		
		movsd 	xmm1, [offsetx]
		addsd 	xmm1, xmm3
		movsd 	[offsetx], xmm1
   
	; y tengely szerint mozgatás, eltolás frissités
		vcvtsi2sd 		xmm3, edi
		mulsd 			xmm3, [zoomy]
		vcvtsi2sd 		xmm0, [height]
		divsd 			xmm3, xmm0

		movsd 	xmm1, [offsety]
		addsd 	xmm1, xmm3
		movsd 	[offsety], xmm1
  
	; frissítem a mandelbrot ciklus határát
		mov 	eax, [hatarvalt]
		add 	[hatar], eax

    jmp     .mainloop
   
    ; Exit
.end:
    call    gfx_destroy
    ret
   
 
		
SingleKoordinata:	

	push	edx
	mov		[x_koord], edx
	mov		[y_koord], ecx
	inc 	edx
	mov		[x_koord+4], edx
	mov		[y_koord+4], ecx
	inc 	edx
	mov		[x_koord+8], edx
	mov		[y_koord+8], ecx
	inc 	edx
	mov		[x_koord+12], edx
	mov		[y_koord+12], ecx
	inc 	edx
	mov		[x_koord+16], edx
	mov		[y_koord+16], ecx
	inc 	edx
	mov		[x_koord+20], edx
	mov		[y_koord+20], ecx
	inc 	edx
	mov		[x_koord+24], edx
	mov		[y_koord+24], ecx
	inc 	edx
	mov		[x_koord+28], edx
	mov		[y_koord+28], ecx
	inc		edx
	pop		edx
	vcvtdq2ps	 ymm0, [x_koord]
	vcvtdq2ps	 ymm1, [y_koord]

	; ymm0 - x és ymm1 - y 		
	
	; elosztom a méterettel, hogy 0-1 között legyen a koordináta tengely
		vcvtdq2ps 		ymm2, [width]
		vcvtdq2ps	 	ymm3, [height]
		vdivps 		ymm0, ymm2
		vdivps 		ymm1, ymm3
	
	; levonok felet, hogy középen legyen majd a nagyítás
		movsd 		 xmm4, [szam_0_5]
		cvtpd2ps	 xmm4, xmm4
		vbroadcastss ymm4, xmm4
		vsubps 		 ymm0, ymm4
		vsubps 		 ymm1, ymm4
	
	; beszorzom a nagyítással
		movsd 		 xmm5, [zoomx]
		movsd 		 xmm6, [zoomy]
		cvtpd2ps 	 xmm5, xmm5
		cvtpd2ps 	 xmm6, xmm6
		vbroadcastss ymm5, xmm5
		vbroadcastss ymm6, xmm6
		vmulps 		 ymm0, ymm5
		vmulps 		 ymm1, ymm6
		
	; hozzáadom az eltolásokat
		movsd 		 xmm2, [offsetx]
		movsd 		 xmm3, [offsety]
		cvtpd2ps 	 xmm2, xmm2
		cvtpd2ps 	 xmm3, xmm3
		vbroadcastss ymm2, xmm2
		vbroadcastss ymm3, xmm3
		vaddps 		 ymm0, ymm2
		vaddps 		 ymm1, ymm3
		
	ret
			
SingleSzamol:
	; ymm0		valos resz					;	a0		a1 ...
	; ymm1      img  resz					;  	b0		b1 ...

	movsd		 xmm6, [szam_4]
	cvtpd2ps	 xmm6, xmm6
    vbroadcastss ymm6, xmm6					;	4		4 ...
	vpxor		 ymm2, ymm2					; 	0		0 ...
	vpxor		 ymm3, ymm3 				;	0		0 ...
	
	push	ecx
	push	eax
	xor		ecx, ecx
	vpxor		ymm7, ymm7

	.ciklus:	
		cmp		ecx, CIKLUSVEGE			;majd ATKELL IRNI
		jge		.vege
		inc		ecx
		vmovups		ymm4, ymm2				;	x0		x1 ...
		vmulps		ymm4, ymm3				;	x0*y0	x1*y1 ...
		vmulps		ymm2, ymm2				;	x0^2	x1^2 ...
		vmulps		ymm3, ymm3				;	y0^2	y1^2 ...
		vmovups		ymm5, ymm2
		vaddps		ymm5, ymm3				; 	x0^2+y0^2 ...
		vsubps		ymm2, ymm3				; 	x0^2-y0^2 ...
		vaddps		ymm2, ymm0				;   x0 ...
		vmovups		ymm3, ymm4				;   x0*y0 ...
		vaddps		ymm3, ymm3				; 	2x0*y0 ...
		vaddps		ymm3, ymm1				;	y0 ...
		vcmpngeps	ymm5, ymm6, ymm5		; hasonlitja, hogy átlépték-e már a 4-et
	
	; maszkolom ymm7-et eax-be és ha 0 lett akkor kilépek a ciklusból, hogy ne kelljen a végéig mennie 
		movsd	 	 xmm4, [szam_1]			
		cvtpd2ps	 xmm4, xmm4
		vbroadcastss ymm4, xmm4
		vaddps 		 ymm7, ymm4
		vandps 		 ymm7, ymm5
		
		jmp .ciklus
	
	.vege:
		pop	eax
		pop ecx
		ret
		
SingleSzinez:
	push	eax
	push	ebx

	; ymm7-ben megkapom hogy az iteráció melyik pixelre milyen értéket számolt majd ezt átalakítom egész számokra
		vcvtps2dq	ymm7, ymm7		; ymm7 - kék és ugyanakkor a 8 pixel minden bitje is majd ide kerül
	
	; frissitem a szint, ha átlépte a 255-öt levonok belőle
		push	edx					
		mov		edx, [color]		; edx - jelenlegi szín változás (10 ha változott, 0 ha nop)
		mov		ebx, [colorall]		; ebx - előző szín 
		add		ebx, edx			; hozzáadom az előző színhez a változást
		cmp		ebx, 255			; ha átlépi a 255-öt levonok belőle
		jl		.tovabb
		sub		ebx, 255
		.tovabb:
		mov		[colorall], ebx		; visszamentem az aktuális színt
		pop		edx

	; az aktuális szín bekerül a szineket tároló regiszterekbe
		mov				[mem], ebx
		vpbroadcastd	ymm2, [mem]
		vpaddd			ymm7, ymm2		
		vmovups			ymm6, ymm7		; ymm6 - zöld
		vmovups			ymm5, ymm7		; ymm5 - piros
	
	; eltolom a zöld 8-al, a pirosat 16-al
		mov				[mem], word 256
		vpbroadcastd	ymm3, [mem]
		vpmulld			ymm6, ymm3
		vpmulld			ymm5, ymm3
		vpmulld			ymm5, ymm3
	
	; zöld
		mov				[mem], byte 6
		vpbroadcastd	ymm4, [mem]
		vpmulld			ymm6, ymm4
		vpaddd			ymm7, ymm6
	
	; piros
		mov				[mem], byte 18
		vpbroadcastd	ymm4, [mem]
		vpmulld			ymm5, ymm4
		vpaddd			ymm7, ymm5
	
	; a gfx tömbbe kerül az ymm7 amiben 8 pixel szinezése van
		vmovups		[eax], ymm7
	
	.vege:
		pop		ebx
		pop		eax
		add		eax, 32	
		ret
		
DoubleKoordinata:	
	
	push	edx
	mov		[x_koord], edx
	mov		[y_koord], ecx
	inc 	edx
	mov		[x_koord+4], edx
	mov		[y_koord+4], ecx
	inc 	edx
	mov		[x_koord+8], edx
	mov		[y_koord+8], ecx
	inc 	edx
	mov		[x_koord+12], edx
	mov		[y_koord+12], ecx
	inc		edx
	pop		edx
	vcvtdq2pd	 ymm0, [x_koord]
	vcvtdq2pd	 ymm1, [y_koord]
	
	; ymm0 - x és ymm1 - y 		
	
	; elosztom a méterettel, hogy 0-1 között legyen a koordináta tengely	
		vcvtdq2pd 		ymm2, [width]
		vcvtdq2pd	 	ymm3, [height]
		vdivpd 			ymm0, ymm2
		vdivpd 			ymm1, ymm3
	
	; levonok felet, hogy középen legyen majd a nagyítás
		movsd	 		xmm4, [szam_0_5]
		vbroadcastsd	ymm4, xmm4
		vsubpd 			ymm0, ymm4
		vsubpd 			ymm1, ymm4
	
	; beszorzom a nagyítással
		movsd 		 xmm5, [zoomx]
		movsd 		 xmm6, [zoomy]
		vbroadcastsd ymm5, xmm5
		vbroadcastsd ymm6, xmm6
		vmulpd 		 ymm0, ymm5
		vmulpd 		 ymm1, ymm6
	
	; hozzáadom az eltolásokat
		movsd 		 xmm2, [offsetx]
		movsd 		 xmm3, [offsety]
		vbroadcastsd ymm2, xmm2
		vbroadcastsd ymm3, xmm3
		vaddpd 		 ymm0, ymm2
		vaddpd 		 ymm1, ymm3
		
	ret
			
DoubleSzamol:
	; ymm0		valos resz					;	a0		a1 ...
	; ymm1      img  resz					;  	b0		b1 ...
	
	movsd			xmm6, [szam_4]
	vbroadcastsd	ymm6, xmm6
	
	vpxor		ymm2, ymm2					; 	0		0 ...
	vpxor		ymm3, ymm3 					;	0		0 ...
	
	push 	ecx
	push	eax
	xor		ecx, ecx
	vpxor	ymm7, ymm7
	
	.ciklus:	
		cmp		ecx, CIKLUSVEGE				; MAJD ATKELL IRNI
		jge		.vege
		inc		ecx
		vmovupd		ymm4, ymm2				;	x0		x1 ...
		vmulpd		ymm4, ymm3				;	x0*y0	x1*y1 ...
		vmulpd		ymm2, ymm2				;	x0^2	x1^2 ...
		vmulpd		ymm3, ymm3				;	y0^2	y1^2 ...
		vmovupd		ymm5, ymm2
		vaddpd		ymm5, ymm3				; 	x0^2+y0^2 ...
		vsubpd		ymm2, ymm3				; 	x0^2-y0^2 ...
		vaddpd		ymm2, ymm0				;   x0' ...
		vmovupd		ymm3, ymm4				;   x0*y0 ...
		vaddpd		ymm3, ymm3				; 	2x0*y0 ...
		vaddpd		ymm3, ymm1				;	y0' ...
		vcmpngeps	ymm5, ymm6, ymm5		; hasonlitja, hogy átlépték-e már a 4-et
	
	; maszkolom ymm7-et eax-be és ha 0 lett akkor kilépek a ciklusból, hogy ne kelljen a végéig mennie
		movsd	 	xmm4, [szam_1]
		vbroadcastsd	ymm4, xmm4
		vaddpd 		ymm7, ymm4
		vandpd 		ymm7, ymm5
		vmovmskpd	eax, ymm7
		jmp .ciklus
	
	.vege:
		pop		eax
		pop		ecx
		ret
		
DoubleSzinez:
	push	eax
	push	ebx
	
	; ymm7-ben megkapom hogy az iteráció melyik pixelre milyen értéket számolt majd ezt átalakítom egész számokra
		vcvtpd2dq	xmm7, ymm7		; xmm7 - kék és ugyanakkor a 4 pixel minden bitje is majd ide kerül
	
	; frissitem a szint, ha átlépte a 255-öt levonok belőle
		push	edx					
		mov		edx, [color]		; edx - jelenlegi szín változás (10 ha változott, 0 ha nop)
		mov		ebx, [colorall]		; ebx - előző szín 
		add		ebx, edx			; hozzáadom az előző színhez a változást
		cmp		ebx, 255			; ha átlépi a 255-öt levonok belőle
		jl		.tovabb
		sub		ebx, 255
		.tovabb:
		mov		[colorall], ebx		; visszamentem az aktuális színt
		pop		edx
		
	; az aktuális szín bekerül a szineket tároló regiszterekbe
		mov				[mem], ebx
		vpbroadcastd	xmm4, [mem]
		paddd			xmm7, xmm4
		movups			xmm6, xmm7		; xmm6 - zöld
		movups			xmm5, xmm7		; xmm5 - piros
		
	; eltolom a zöld 8-al, a pirosat 16-al
		mov				[mem], word 256
		vpbroadcastd	xmm4, [mem]
		pmulld			xmm6, xmm4
		pmulld			xmm5, xmm4
		pmulld			xmm5, xmm4
		
	; zöld
		mov				[mem], byte 6
		vpbroadcastd	xmm4, [mem]
		pmulld			xmm6, xmm4
		paddd			xmm7, xmm6

	; piros
		mov				[mem], byte 18
		vpbroadcastd	xmm4, [mem]
		pmulld			xmm5, xmm4
		paddd			xmm7, xmm5
	
	; a gfx tömbbe kerül az xmm7 amiben 4 pixel szinezése van
		movups		[eax], xmm7
	
	.vege:
		pop		ebx
		pop		eax
		add		eax, 16
		ret
		
	
section .data
	x_koord 		dd	0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
	y_koord 		dd	0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
	szam_0_5 		dq 	0.5
	szam_1			dq	1.0
	szam_4			dq	4.0
	width 			dd 	1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024
    height			dd 	768, 768, 768, 768, 768, 768, 768, 768
	zoomx			dq 	4.0
	zoomy 			dq 	3.0
	seged 			dq  0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
		
	nagyit 			dd 	100
	kicsinyit		dd	100
	
	hatar			dd	50
	hatarvalt		dd 	0
	
	color			dd  0
	colorall		dd  0
	
	pontossag		dd 	1
	
	offsetx 		dq 	0
    offsety 		dq 	0
	
	caption db "Assembly Graphics Demo", 0
	infomsg db "WASD mozgas, 1/2 pontossag, 3 szin, Q/E zoom, Z/X iteracio noveles/csokkentes", 0
	errormsg db "ERROR: could not initialize graphics!", 0
	
section .bss
	mem		resw	4
	
	
	
	
	
	
	
	
	
	
	
	
	
	
